class Solution {

    public static void main(String [] args){
        List<List<Integer>> result = new Solution().generate(5);
        for (int i = 0; i < result.size(); i++) {
            for (Integer integer : result.get(i)) {
                System.out.println(integer);
            }
        }
    }

    private List<List<Integer>> generate(int numRows){
        List<List<Integer>> result = new ArrayList<>();
        for (int i = 0; i < numRows; i++) {
            List<Integer> list = new ArrayList<>();
            for (int j=0; j<i+1;j++){
                if (j==0||j==i){
                    list.add(1);
                }else{
                    int left = result.get(i-1).get(j-1);
                    int right = result.get(i-1).get(j);
                    list.add(left+right);
                }
            }
            result.add(list);
        }
        return result;

    }
}